import React from 'react';
import Sidebar from '../Component/Sidebar/Sidebar';
import Timebar from '../Component/Timebar/Timebar';
import FooterNav from '../Component/FooterNav/FooterNav';
import Routing from '../Router/Routing';

const Layout = () => {
  return (
    <div>
      <Sidebar />
      <Timebar />
      <Routing />
      <FooterNav />
    </div>
  );
};

export default Layout;
