import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Orders from '../Pages/Orders';
import Products from '../Pages/Products';
// import Front from '../Component/Home/Front';
// import TopNav from '../Component/TopNav/TopNav';
// import FrontBar from '../Component/FrontBar/FrontBar';
import Home from '../Pages/Home';
const Routing = () => {
  return (
    <BrowserRouter>
    <Routes>
    <Route path="/" element={<Home />} />
        <Route path="/orders" element={<Orders />} />
        <Route path="/products" element={<Products />} />
    </Routes>
    </BrowserRouter>
  );
};

export default Routing;

