import React, { useState, useEffect } from 'react';
import './Button.css';
// import Dropzone from 'react-dropzone';

const Table = () => {
  const [products, setProducts] = useState([]);
  const [showPopup, setShowPopup] = useState(false);
  const [formData, setFormData] = useState({
    productName: '',
    variant: '',
    price: '',
  });
  const [currentProduct, setCurrentProduct] = useState(null);
  const [isEditing, setIsEditing] = useState(false);

  // Function to save products to local storage
  const saveProductsToLocalStorage = (products) => {
    localStorage.setItem('products', JSON.stringify(products));
  };

  // Function to get products from local storage
  const getProductsFromLocalStorage = () => {
    const storedProducts = localStorage.getItem('products');
    return storedProducts ? JSON.parse(storedProducts) : [];
  };

  // Load products from local storage on component mount
  useEffect(() => {
    const storedProducts = getProductsFromLocalStorage();
    setProducts(storedProducts);
  }, []);

  // Function to handle adding a new product
  const handleAddProduct = () => {
    if (isEditing && currentProduct) {
      // Edit the current product
      const updatedProducts = products.map((product) =>
        product.id === currentProduct.id ? { ...product, ...formData } : product
      );

      setProducts(updatedProducts);
      setCurrentProduct(null);
      setIsEditing(false);
    } else {
      // Add a new product
      const newProduct = { ...formData, id: new Date().getTime() };
      const updatedProducts = [...products, newProduct];
      setProducts(updatedProducts);
    }

    saveProductsToLocalStorage(products);
    setFormData({ productName: '', variant: '', price: '' });
    togglePopup();
  };
  const togglePopup = () => {
    setShowPopup(!showPopup);
  };

  // Function to handle editing a product
  const handleEditProduct = (product) => {
    setCurrentProduct(product);
    setFormData(product);
    setIsEditing(true);
    togglePopup();
  };
  const handleDeleteProduct = (productToDelete) => {
    const updatedProducts = products.filter((product) => product.id !== productToDelete.id);
    setProducts(updatedProducts);
    saveProductsToLocalStorage(updatedProducts);
  };
  // Function to handle canceling an edit or new product
  const handleCancel = () => {
    setCurrentProduct(null);
    setIsEditing(false);
    setFormData({ productName: '', variant: '', price: '' });
    togglePopup();
  };

  // ... Existing code ...

  return (
    <div className="Table">
      <div className="Add-Products">
        <button className="Btn" onClick={togglePopup}>
          Add Product
        </button>
      </div>
      <table>
        {/* ... Table headers and rows ... */}
        <tbody>
          {products.map((product) => (
            <tr key={product.id}>
              {/* ... Table data cells ... */}
              <td>
                <button onClick={() => handleEditProduct(product)}>Edit</button>
              </td>
              <td>
                <button onClick={() => handleDeleteProduct(product)}>Delete</button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>

      {showPopup && (
        <div className="popup">
          <div className="popup_inner">
            <h2>{isEditing ? 'Edit Product' : 'Add Product'}</h2>
            {/* ... Input fields for product details ... */}
            <button onClick={handleAddProduct}>{isEditing ? 'Save' : 'Add'}</button>
            <button onClick={handleCancel}>Cancel</button>
          </div>
        </div>
      )}
    </div>
  );
};

export default Table;
