import React, { useState, useEffect } from 'react';
import '../Order/Order.css';
import NavBar from '../NavBar/NavBar';
import moment from 'moment';

const OrderComponent = () => {
  const dummyOrdersData = [
    {
      id: 1,
      date: '2023-07-22',
      customerName: 'Jayshiva',
      location: 'Madurai',
      amount: 100,
      statusOrder: 'Pending',
    },
    {
      id: 2,
      date: '2023-07-23',
      customerName: 'Angel',
      location: 'VNR',
      amount: 75,
      statusOrder: 'Prepared',
    },
    {
      id: 3,
      date: '2023-07-24',
      customerName: 'Mareesh',
      location: 'VNR',
      amount: 150.0,
      statusOrder: 'Delivered',
    },
    {
      id: 4,
      date: '2023-07-25',
      customerName: 'Amarnath',
      location: 'Madurai',
      amount: 200.0,
      statusOrder: 'Pending',
    },
    {
      id: 5,
      date: '2023-07-27',
      customerName: 'Shiva',
      location: 'Madurai',
      amount: 200.0,
      statusOrder: 'Cancelled',
    },
    {
      id: 6,
      date: '2023-07-26',
      customerName: 'Gayathri',
      location: 'Sivakasi',
      amount: 120.0,
      statusOrder: 'Prepared',
    },
    {
      id: 7,
      date: '2023-07-28',
      customerName: 'Suriya',
      location: 'Paris',
      amount: 50.0,
      statusOrder: 'Cancelled',
    },
    {
      id: 8,
      date: '2023-07-29',
      customerName: 'Ishwarya',
      location: 'Madurai',
      amount: 300.0,
      statusOrder: 'Delivered',
    },
    {
      id: 9,
      date: '2023-07-30',
      customerName: 'Rashika',
      location: 'Madurai',
      amount: 180.0,
      statusOrder: 'Cancelled',
    },
    {
      id: 10,
      date: '2023-07-31',
      customerName: 'Shakthi',
      location: 'Sivagangai',
      amount: 220.0,
      statusOrder: 'Prepared',
    },
  ];

  const [dummyOrders, setDummyOrders] = useState(dummyOrdersData);
  const [selectedStatus, setSelectedStatus] = useState(null);
  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');
  const [filteredData, setFilteredData] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [currentStatus, setCurrentStatus] = useState('');
  const [orderIdToUpdate, setOrderIdToUpdate] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setFilteredData(dummyOrders);
    handleFilterOrders(selectedStatus, startDate, endDate);
  }, [selectedStatus, startDate, endDate]);

  const handleStatusClick = (status) => {
    setSelectedStatus(status);
  };

  const handleDateFilter = (start, end) => {
    setStartDate(start);
    setEndDate(end);
  };

  const handleFilterOrders = (status, startDate, endDate) => {
    let filteredData = dummyOrders;

    if (status) {
      filteredData = filteredData.filter((order) => order.statusOrder === status);
    }

    if (startDate && endDate) {
      const startDateObj = new Date(startDate);
      const endDateObj = new Date(endDate);
      filteredData = filteredData.filter((order) => {
        const orderDate = new Date(order.date);
        return orderDate >= startDateObj && orderDate <= endDateObj;
      });
    }

    setFilteredData(filteredData);
  };

  const handleSaveStatus = () => {
    const updatedData = dummyOrders.map((order) =>
      order.id === orderIdToUpdate ? { ...order, statusOrder: currentStatus } : order
    );

    setFilteredData(updatedData);
    setDummyOrders(updatedData);
    setShowModal(false);
  };

  const handleEditFilter = (orderId) => {
    setIsLoading(true);
    setOrderIdToUpdate(orderId);

    const order = filteredData.find((order) => order.id === orderId);
    setCurrentStatus(order.statusOrder);

    setTimeout(() => {
      setIsLoading(false);
      setShowModal(true);
    }, 400); 
  };

  return (
    <div>
      <NavBar handleStatusClick={handleStatusClick} handleDateFilter={handleDateFilter} />

      <div>
        <table>
          <thead>
            <tr>
              <th>Order ID</th>
              <th>Date</th>
              <th>Customer Name</th>
              <th>Location</th>
              <th>Amount</th>
              <th>Status Order</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {filteredData.length === 0 ? (
              <tr>
                <td colSpan="7" style={{ textAlign: "center" }}>
                  No orders right now!!
                </td>
              </tr>
            ) : (
              filteredData.map((order) => (
                <tr key={order.id}>
                  <td>{order.id}</td>
                  <td>{moment(order.date).format('DD-MM-YYYY')}</td>
                  <td>{order.customerName}</td>
                  <td>{order.location}</td>
                  <td>₹{order.amount.toFixed(2)}</td>
                  <td>{order.statusOrder}</td>
                  <td>
                    <button onClick={() => handleEditFilter(order.id)}>Edit Filter</button>
                  </td>
                </tr>
              ))
            )}
          </tbody>
        </table>
      </div>

      {isLoading && (
        <div className="loading-overlay">
          <div className="spinner"></div>
        </div>
      )}

      {showModal && (
        <div className="modal-overlay">
          <div className="modal">
            <span className="close" onClick={() => setShowModal(false)}>&times;</span>
            <h2>Edit Status</h2>
            <select
              value={currentStatus}
              onChange={(e) => setCurrentStatus(e.target.value)}
            >
              <option value="Pending">Pending</option>
              <option value="Prepared">Prepared</option>
              <option value="Delivered">Delivered</option>
              <option value="Cancelled">Cancelled</option>
            </select>
            <div style={{ display: "flex", justifyContent: "flex-end", marginTop: "20px" }}>
              <button onClick={handleSaveStatus}>Save</button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default OrderComponent;
