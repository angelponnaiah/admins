import React from 'react';
import { faUser, faHeart, faComment, faDollar } from '@fortawesome/free-solid-svg-icons';
import Cards from '../Reusable/Cards/Cards'

const FrontBar = () => {
  // Data for the cards
  const cardsData = [
    {
      icon: faDollar,
      count: 100,
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    },
    {
      icon: faHeart,
      count: 50,
      description: 'Nulla eget lectus eu nulla facilisis sodales.',
    },
    {
      icon: faComment,
      count: 200,
      description: 'Pellentesque quis mi quis urna aliquam eleifend.',
    },
  ];

  return (
    <div>
      {cardsData.map((card, index) => (
        <Cards key={index} {...card} />
      ))}
    </div>
  );
};

export default FrontBar;
