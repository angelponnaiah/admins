import React, { useState } from 'react'
import './TopNav.css';
import { FaBook, FaRupeeSign, FaUser } from "react-icons/fa";

const TopNav = () => {
const data = [
  {
    id: 1,
    date: '2023-07-22',
    customerName: 'Jayshiva',
    location: 'Madurai',
    amount: 100,
  },
  {
    id: 2,
    date: '2023-07-23',
    customerName: 'Angel',
    location: 'VNR',
    amount: 75,
  },
  {
    id: 3,
    date: '2023-07-24',
    customerName: 'Mareesh',
    location: 'VNR',
    amount: 150.0,
  },
  {
    id: 4,
    date: '2023-07-25',
    customerName: 'Amarnath',
    location: 'Madurai',
    amount: 200.0,
  },
  {
    id: 5,
    date: '2023-07-27',
    customerName: 'Shiva',
    location: 'Madurai',
    amount: 200.0,
  },
  {
    id: 6,
    date: '2023-07-26',
    customerName: 'Gayathri',
    location: 'Sivakasi',
    amount: 120.0,
  },
  {
    id: 7,
    date: '2023-07-28',
    customerName: 'Suriya',
    location: 'Paris',
    amount: 50.0,
  },
  {
    id: 8,
    date: '2023-07-29',
    customerName: 'Ishwarya',
    location: 'Madurai',
    amount: 300.0,
  },
  {
    id: 9,
    date: '2023-07-30',
    customerName: 'Rashika',
    location: 'Madurai',
    amount: 180.0,
  },
  {
    id: 10,
    date: '2023-07-31',
    customerName: 'Shakthi',
    location: 'Sivagangai',
    amount: 220.0,
  },
  {
    id: 11,
    date: '2023-07-22',
    customerName: 'Jayshiva',
    location: 'Madurai',
    amount: 100,
  
  },
  {
    id: 12,
    date: '2023-07-23',
    customerName: 'Angel',
    location: 'VNR',
    amount: 75,
  },
  {
    id: 13,
    date: '2023-07-24',
    customerName: 'Mareesh',
    location: 'VNR',
    amount: 150.0,
  },
  {
    id: 14,
    date: '2023-07-25',
    customerName: 'Amarnath',
    location: 'Madurai',
    amount: 200.0,
  },
  {
    id: 15,
    date: '2023-07-27',
    customerName: 'Shiva',
    location: 'Madurai',
    amount: 200.0,
  },
  {
    id: 16,
    date: '2023-07-26',
    customerName: 'Gayathri',
    location: 'Sivakasi',
    amount: 120.0,

  },
  {
    id: 17,
    date: '2023-07-28',
    customerName: 'Suriya',
    location: 'Paris',
    amount: 50.0,

  },
  {
    id: 18,
    date: '2023-07-29',
    customerName: 'Ishwarya',
    location: 'Madurai',
    amount: 300.0,
  
  },
  {
    id: 19,
    date: '2023-07-30',
    customerName: 'Rashika',
    location: 'Madurai',
    amount: 180.0,
    
  },
  {
    id: 20,
    date: '2023-07-31',
    customerName: 'Shakthi',
    location: 'Sivagangai',
    amount: 220.0,
  },
];
const dummyOrdersData = [
  {
     id: 1,
     date: '2023-07-22',
     customerName: 'Jayshiva',
     location: 'Madurai',
     amount: 100,
     statusOrder: 'Pending',
   },
   {
     id: 2,
     date: '2023-07-23',
     customerName: 'Angel',
     location: 'VNR',
     amount: 75,
     statusOrder: 'Prepared',
   },
   {
     id: 3,
     date: '2023-07-24',
     customerName: 'Mareesh',
     location: 'VNR',
     amount: 150.0,
     statusOrder: 'Delivered',
   },
   {
     id: 4,
     date: '2023-07-25',
     customerName: 'Amarnath',
     location: 'Madurai',
     amount: 200.0,
     statusOrder: 'Pending',
   },
   {
     id: 5,
     date: '2023-07-27',
     customerName: 'Shiva',
     location: 'Madurai',
     amount: 200.0,
     statusOrder: 'Cancelled',
   },
   {
     id: 6,
     date: '2023-07-26',
     customerName: 'Gayathri',
     location: 'Sivakasi',
     amount: 120.0,
     statusOrder: 'Prepared',
   },
   {
     id: 7,
     date: '2023-07-28',
     customerName: 'Suriya',
     location: 'Paris',
     amount: 50.0,
     statusOrder: 'Cancelled',
   },
   {
     id: 8,
     date: '2023-07-29',
     customerName: 'Ishwarya',
     location: 'Madurai',
     amount: 300.0,
     statusOrder: 'Delivered',
   },
   {
     id: 9,
     date: '2023-07-30',
     customerName: 'Rashika',
     location: 'Madurai',
     amount: 180.0,
     statusOrder: 'Cancelled',
   },
   {
     id: 10,
     date: '2023-07-31',
     customerName: 'Shakthi',
     location: 'Sivagangai',
     amount: 220.0,
     statusOrder: 'Prepared',
   },
 ];

 const totalDeliveredOrders = dummyOrdersData.filter(
  (order) => order.statusOrder === "Delivered"
).length;

const totalCancelOrders = dummyOrdersData.filter(
  (order) => order.statusOrder === "Cancelled"
).length;


const totalAmountOfDeliveredOrders = dummyOrdersData
  .filter((order) => order.statusOrder === "Delivered")
  .reduce((total, order) => total + order.amount, 0);

 

  return (
   
    <div className='Page'>
    <div className='DashBoard'>
      <div className='TopDash'>
        <div className='DashHead'>
          <h4>DashBoard</h4>
          
        </div>
<div className='DashFilter'>  
        
        </div>
      </div>

      <div className='BottomDash'>
        <div className='MenuCard'>
        <div className='icon' ><FaBook  size={15} color='blue'/></div>
          <div className='Menu'>
             <h4>{data.length}</h4>
             <h6>Total Products</h6>
          </div>
         
          </div>
          <div className='MenuCard'>
        <div className='icon'> <FaRupeeSign  size={15} color='blue'/></div>
          <div className='Menu'>
            <h4>{totalAmountOfDeliveredOrders}</h4>
            <h6>Total Revenue</h6>
          </div></div>


          <div className='MenuCard'>
          <div className='icon'><FaUser  size={15} color='blue'/></div>
          <div className='Menu'>
          
          <h4>{totalDeliveredOrders}</h4>
            <h6>Total Order</h6>
          </div></div>

          <div className='MenuCard'>
          <div className='icon'><FaUser  size={15} color='blue'/></div>
          <div className='Menu'>
          
          <h4>{totalCancelOrders}</h4>
            <h6>Cancel Order</h6>
          </div></div>
      </div>
      
    </div>
    </div>
  )
}
 
export default TopNav
