import React from 'react';
import './FooterNav.css';

const FooterNav = () => {
  return (
    <nav className="down-navbar">
      <div className="left-content">
        Showing 1 to 10 of 30 entries
      </div>
      <div className="right-content">
        <button className="prev-btn">Previous</button>
        <div className="page-numbers">
          <button>1</button>
          <button>2</button>
          <button>3</button>
        </div>
        <button className="next-btn">Next</button>
      </div>
    </nav>
  );
};

export default FooterNav;
// FooterNav.js
// import React, { useState } from 'react';
// import { Link } from 'react-router-dom';
// import './FooterNav.css';


// const FooterNav = () => {
//   const totalPages = 3; // Total number of pages
//   const [currentPage, setCurrentPage] = useState(1); // State to keep track of the current page

//   // Function to handle the "Next" button click
//   const handleNextClick = () => {
//     const nextPage = currentPage + 1;
//     if (nextPage <= totalPages) {
//       setCurrentPage(nextPage);
//     }
//   };

//   // Function to handle the "Previous" button click
//   const handlePreviousClick = () => {
//     const previousPage = currentPage - 1;
//     if (previousPage >= 1) {
//       setCurrentPage(previousPage);
//     }
//   };
  
//   return (
//     <nav className="down-navbar">
//     <div className="left-content">
//       Showing 1 to 10 of 30 entries
//     </div>
//     <div className="right-content">
//       <button className="prev-btn" onClick={handlePreviousClick}>Previous</button>
//       <div className="page-numbers">
//         {/* Generate the page number links dynamically */}
//         {Array.from({ length: totalPages }).map((_, index) => (
//           <Link key={index} to={`./pages/${index + 1}`} className={currentPage === index + 1 ? 'active' : ''}>
//             {index + 1}
//           </Link>
//         ))}
//       </div>
//       <button className="next-btn" onClick={handleNextClick}>Next</button>
//     </div>
//   </nav>
//   );
// };

// export default FooterNav;

