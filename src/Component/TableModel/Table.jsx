import React, { useState, useEffect } from 'react';
import './Table.css';
import Dropzone from 'react-dropzone';

// import CircleLoader from './Component/Loader/CircleLoader';

const Table = () => {
  const [products, setProducts] = useState([]);
  const [showPopup, setShowPopup] = useState(false);
  const [searchTerm, setSearchTerm] = useState('');
  const [formData, setFormData] = useState({
    productName: '',
    variant: '',
    price: '',
  });
  const [currentProduct, setCurrentProduct] = useState(null);
  const [isEditing, setIsEditing] = useState(false);
  const filteredProducts = products.filter((product) =>
    product.productName.toLowerCase().includes(searchTerm.toLowerCase())
  );

  const saveProductsToLocalStorage = (products) => {
    localStorage.setItem('products', JSON.stringify(products));
  };
  const getProductsFromLocalStorage = () => {
    const storedProducts = localStorage.getItem('products');
    return storedProducts ? JSON.parse(storedProducts) : [];
  };
  useEffect(() => {
    const storedProducts = getProductsFromLocalStorage();
    setProducts(storedProducts);
    
  }, []);


  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleAddProduct = () => {
    if (isEditing && currentProduct) {
      // Edit the current product
      const updatedProducts = products.map((product) =>
        product.id === currentProduct.id ? { ...product, ...formData } : product
      );

      setProducts(updatedProducts);
      setCurrentProduct(null); //indicating that the editing process is complete, and no product is being edited
      setIsEditing(false); // editing mode is turned off.
    } else {
      // Add a new product
      const newProduct = { ...formData, id: new Date().getTime() };
      const updatedProducts = [...products, newProduct];
      setProducts(updatedProducts);
    }

    saveProductsToLocalStorage(products);
    setFormData({ productName: '', variant: '', price: '' });
    togglePopup();
  };

  const handleImageDrop = (acceptedFiles, rowIndex) => {
    if (acceptedFiles.length > 0) {
      const imageFile = acceptedFiles[0];

      // Check if the file is an image (you can add more allowed image types if needed)
      const allowedImageTypes = ['image/jpeg', 'image/png', 'image/gif'];
      if (allowedImageTypes.includes(imageFile.type)) {
        const updatedProducts = [...products];
        updatedProducts[rowIndex].image = imageFile;
        setProducts(updatedProducts);
      } else {
        alert('Please upload a valid image file (JPEG, PNG, or GIF).');
      }
    }
  };


  const handleImageDrag = (event, rowIndex) => {
    event.dataTransfer.setData('text/plain', rowIndex.toString());
  };

  const handleImageDropOnRow = (event, rowIndex) => {
    event.preventDefault();
    const sourceRowIndex = parseInt(event.dataTransfer.getData('text'));
    if (sourceRowIndex !== rowIndex) {
      const updatedProducts = [...products];
      const temp = updatedProducts[sourceRowIndex].image;
      updatedProducts[sourceRowIndex].image = updatedProducts[rowIndex].image;
      updatedProducts[rowIndex].image = temp;
      setProducts(updatedProducts);
    }
  };

  const togglePopup = () => {
    setShowPopup(!showPopup);
  };
  const handleEditProduct = (product) => {
    setCurrentProduct(product);
    setFormData(product);
    setIsEditing(true);
    togglePopup();
  };
  const handleDeleteProduct = (productToDelete) => {
    const updatedProducts = products.filter((product) => product.id !== productToDelete.id);
    setProducts(updatedProducts);
    saveProductsToLocalStorage(updatedProducts);
  };
  const handleCancel = () => {
    setCurrentProduct(null);
    setIsEditing(false);
    setFormData({ productName: '', variant: '', price: '' });
    togglePopup();
  };


  return (
    <div className="Table">
      <div className="Add-Products">
        <h3>Products</h3>
        <input class="search1"
          type="text"
          placeholder="Search product name..."
          value={searchTerm}
          onChange={(e) => setSearchTerm(e.target.value)}
        />
        <button className="Btn" onClick={togglePopup}>
          Add Product
        </button>
      </div>
      <table>
        <thead>
          <tr>
            <th>Product ID</th>
            <th>Product Name</th>
            <th>Variant</th>
            <th>Price</th>
            <th>Image</th>
            <th>Best Seller</th>
            <th>Active</th>
          </tr>
        </thead>
        <tbody>
          {filteredProducts.length === 0 ? (
              <tr>
                <td colSpan="7" style={{ textAlign: "center" }}>
                  No orders right now!!
                </td>
              </tr>
            ) : (
            filteredProducts.map((product, index) => (

              <tr
                key={index}
                onDragOver={(event) => event.preventDefault()}
                onDrop={(event) => handleImageDropOnRow(event, index)}
              >
                <td>{index + 1}</td>
                <td>{product.productName}</td>
                <td>{product.variant}</td>
                <td>{product.price}</td>
                <td>
                  {product.image ? (
                    <img
                      className="uploaded-image"
                      src={product.image instanceof File ? URL.createObjectURL(product.image) : product.image}
                      alt="Uploaded"
                      style={{
                        width: '80px',
                        height: '80px',
                        borderRadius: '50%',
                        objectFit: 'cover',
                      }}
                      draggable
                      onDragStart={(event) => handleImageDrag(event, index)}
                    />
                  ) : (
                    <Dropzone onDrop={(acceptedFiles) => handleImageDrop(acceptedFiles, index)}>
                      {({ getRootProps, getInputProps }) => (
                        <div {...getRootProps()} className="dropzone">
                          <input {...getInputProps()} />
                          <p>Drag</p>
                        </div>
                      )}
                    </Dropzone>
                  )}
                </td>

                <td>TODO</td>
                <td>TODO</td>
                <button onClick={() => handleEditProduct(product)}>Edit</button>
                <button onClick={() => handleDeleteProduct(product)}>Delete</button>
              </tr>
            )))}
        </tbody>
      </table>

      {showPopup && (
        <div className="popup">
          <div className="popup_inner">
            <h2>{isEditing ? 'Edit Product' : 'Add Product'}</h2>
            <label>
              Product Name:
              <input
                type="text"
                name="productName"
                value={formData.productName}
                onChange={handleInputChange}
              />
            </label>
            <label>
              Variant:
              <input
                type="text"
                name="variant"
                value={formData.variant}
                onChange={handleInputChange}
              />
            </label>
            <label>
              Price:
              <input
                type="text"
                name="price"
                value={formData.price}
                onChange={handleInputChange}
              />
            </label>
            <button  onClick={handleAddProduct} >Add</button>
            <button onClick={handleCancel}>Cancel</button>
          </div>
        </div>
      
      )}
    </div>
  
  );
};

export default Table;

