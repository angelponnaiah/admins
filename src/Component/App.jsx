import React, { useState } from 'react';
import NavBar from './NavBar';
import Table from './Table';
import '../App.css';

const App = () => {

  const dummyOrders = [

    {
        id: 1,
        date: '2023-07-24',
        customerName: 'John Doe',
        location: 'New York',
        amount: 100,
        statusOrder: 'pending',
      },
      {
        id: 2,
        date: '2023-07-23',
        customerName: 'Jane Smith',
        location: 'Los Angeles',
        amount: 150,
        statusOrder: 'prepared',
      },
      {
        id: 3,
        date: '2023-07-22',
        customerName: 'Bob Johnson',
        location: 'Chicago',
        amount: 75,
        statusOrder: 'delivered',
      },
      {
        id: 4,
        date: '2023-07-21',
        customerName: 'Alice Williams',
        location: 'Houston',
        amount: 200,
        statusOrder: 'cancelled',
      },
    // Your dummy order data...
  ];

  const [selectedStatus, setSelectedStatus] = useState(null);

  const handleStatusClick = (status) => {
    setSelectedStatus(status);
  };

  return (
    <div className="app">
      <NavBar handleStatusClick={handleStatusClick} />
      <Table orders={dummyOrders} selectedStatus={selectedStatus} />
    </div>
  );
};

export default App;
