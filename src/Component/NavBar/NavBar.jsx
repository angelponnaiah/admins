import React, { useState } from 'react';
import '../NavBar/NavBar.css';

const NavBar = ({ handleStatusClick, handleDateFilter }) => {
  const [showDropdown, setShowDropdown] = useState(false);
  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');

  const toggleDropdown = () => {
    setShowDropdown((prevState) => !prevState);
  };

  const handleStartDateChange = (event) => {
    setStartDate(event.target.value);
  };

  const handleEndDateChange = (event) => {
    setEndDate(event.target.value);
  };

  const handleFilterClick = () => {
    handleDateFilter(startDate, endDate);
  };

  return (
    <div className="navbar">
      <div className="navbar-left">
        <span className="navbar-title">Orders</span>
      </div>
      <div className="navbar-right">
        <button className="status-btn" onClick={toggleDropdown}>
          <i className="fas fa-list-ul"></i> All Status
          {showDropdown && (
            <div className="dropdown-content">
              <a href="#all" onClick={() => handleStatusClick(null)}>All</a>
              <a href="#pending" onClick={() => handleStatusClick('Pending')}>Pending</a>
              <a href="#prepared" onClick={() => handleStatusClick('Prepared')}>Prepared</a>
              <a href="#delivered" onClick={() => handleStatusClick('Delivered')}>Delivered</a>
              <a href="#cancelled" onClick={() => handleStatusClick('Cancelled')}>Cancelled</a>
            </div>
          )}
        </button>
        <div className="date-filter">
          <input type="date" value={startDate} onChange={handleStartDateChange} />
          <input type="date" value={endDate} onChange={handleEndDateChange} />
          <button onClick={handleFilterClick}>Filter</button>
        </div>
      </div>
    </div>
  );
};

export default NavBar;