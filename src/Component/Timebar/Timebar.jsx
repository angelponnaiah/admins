import React, { useEffect, useState } from 'react';
import moment from 'moment';
import './Timebar.css';

const Timebar = () => {
  const [currentTime, setCurrentTime] = useState('');
  const updateDateTime = () => {
    const currentDate = moment().format('MMMM Do YYYY');
    const currentTime = moment().format('h:mm:ss A');
    setCurrentTime(`${currentDate} ${currentTime}`);
  };

  useEffect(() => {
    updateDateTime();
  }, []);

  useEffect(() => {
    const interval = setInterval(updateDateTime, 1000);
    return () => clearInterval(interval);
  }, []);

  const profileImageSrc = 'images/Jayshiva_Passportsizepic.png';
  
  return (
    <div className="top-nav">
    
      <div className="date-time-container">
        <span className="date">{moment().format('MMMM Do YYYY')}</span>
        <span>{moment().format('h:mm:ss A')}</span>
      </div>
      <div className="profile-image">
        <img src={profileImageSrc} alt="Profile" />
      </div>
    </div>
  );
};

export default Timebar;
